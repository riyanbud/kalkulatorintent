package com.example.kalkulatorintent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.kalkulatorintent.R;

public class MainActivity extends AppCompatActivity {

    Button button1, button2, button3, button4, button5, button6, button7, button8, button9, button0;
    Button buttonTambah, buttonKali, buttonKurang, buttonBagi, buttonHasil, buttonC, buttonKoma;
    TextView angka;
    float Bil1, Bil2;
    boolean Tambah, Kurang, Kali, Bagi;
    Context mainActivityContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivityContext = this;
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        button5 = findViewById(R.id.button5);
        button6 = findViewById(R.id.button6);
        button7 = findViewById(R.id.button7);
        button8 = findViewById(R.id.button8);
        button9 = findViewById(R.id.button9);
        button0 = findViewById(R.id.button0);
        buttonTambah = findViewById(R.id.buttonTambah);
        buttonKali = findViewById(R.id.buttonKali);
        buttonKurang = findViewById(R.id.buttonKurang);
        buttonBagi = findViewById(R.id.buttonBagi);
        buttonHasil = findViewById(R.id.buttonHasil);
        buttonC = findViewById(R.id.buttonC);
        buttonKoma = findViewById(R.id.buttonKoma);
        angka = findViewById(R.id.angka);


        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "1");
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "2");
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "3");
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "4");
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "5");
            }
        });
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "6");
            }
        });
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "7");
            }
        });
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "8");
            }
        });
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "9");
            }
        });
        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "0");
            }
        });

        buttonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText("");
            }
        });
        buttonKoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + ".");
            }
        });

        buttonTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (angka == null) {
                    angka.setText("");
                } else {
                    Bil1 = Float.parseFloat(angka.getText() + "");
                    Tambah = true;
                    angka.setText(null);
                }
            }
        });
        buttonKurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bil1 = Float.parseFloat(angka.getText() + "");
                Kurang = true;
                angka.setText(null);
            }
        });
        buttonBagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bil1 = Float.parseFloat(angka.getText() + "");
                Bagi = true;
                angka.setText(null);
            }
        });
        buttonKali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bil1 = Float.parseFloat(angka.getText() + "");
                Kali = true;
                angka.setText(null);
            }
        });

        buttonHasil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bil2 = Float.parseFloat(angka.getText() + "");
                String operasi = "";
                if (Tambah == true) {
                    angka.setText(Bil1 + Bil2 + "");
                    operasi = Bil1 + " + " + Bil2;
                    Tambah = false;
                }
                if (Kurang == true) {
                    angka.setText(Bil1 - Bil2 + "");
                    operasi = Bil1 + " - " + Bil2;
                    Kurang = false;
                }
                if (Bagi == true) {
                    angka.setText(Bil1 / Bil2 + "");
                    operasi = Bil1 + " / " + Bil2;
                    Bagi = false;
                }
                if (Kali == true) {
                    angka.setText(Bil1 * Bil2 + "");
                    operasi = Bil1 + " * " + Bil2;
                    Kali = false;
                }

                Intent intent =new Intent(mainActivityContext, com.example.kalkulatorintent.MainActivity2.class);
                intent.putExtra("Hasil", angka.getText().toString());
                intent.putExtra("Operasi", operasi);
                startActivity(intent);
            }});
    }
}